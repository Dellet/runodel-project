import Vue from 'vue'
import App from './App.vue'
import router from './router'

import "bootstrap-icons/font/bootstrap-icons.css";
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"

import 'font-awesome/scss/font-awesome.scss'

import VueI18n from 'vue-i18n'
import messages from './lang'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'ru',
  fallbackLocale: 'ru',
  messages
})

import ExpandableImage from 'vue-expandable-image'
Vue.use(ExpandableImage)

Vue.config.productionTip = false
new Vue({
  router,
  i18n,
  render: h => h(App),
}).$mount('#app')
