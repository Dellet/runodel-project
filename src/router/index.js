import Vue from 'vue'
import Router from "vue-router";

import Home from '@/components/Home'
import Preloader from "@/components/Preloader.vue";
import AnimatedFilm from "@/components/AnimatedFilm.vue";
import RunesStory from "@/components/RunesStory.vue";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/preloader',
            name: 'preloader',
            component: Preloader
        },
        {
            path: '/animatedfilm',
            name: 'animatedfilm',
            component: AnimatedFilm
        },
        {
            path: '/story',
            name: 'story',
            component: RunesStory
        }
    ],
    base: process.env.BASE_URL,
})

export default router;
